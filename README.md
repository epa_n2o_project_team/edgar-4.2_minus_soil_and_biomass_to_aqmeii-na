*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] and [R][R @ wikipedia] code to

1. sum various [EDGAR-4.2][EDGAR 4.2 overview] N2O emission inventories (in [netCDF][] format). [These inventories][EDGAR categories in this study] cover sectors/processes of N2O production other than those involving biomass burning, oceans, and soils.
1. "reunit": convert from (per-gridcell) flux rate (`kg/m^2/s`) to molar-mass rate (`moles/sec`, which [CMAQ][CMAQ @ CMAS] wants).
1. 2D-regrid from global/unprojected to a projected subdomain ([AQMEII-NA][]).
1. "retemporalize" from annual timestep to hourly (and tweak metadata)
1. check that mass is (more or less) conserved in all of the above

creating CMAQ-style emissions files (e.g., [this][emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz], when `gunzip`ed) containing hourly emissions usable for any day in the year. Currently does not provide a clean or general-purpose (much less packaged) solution! but merely shows how to do these tasks using

* bash (tested with version=3.2.25)
* NCL (tested with version=6.1.2)
* R (tested with version=3.0.0) and packages including
    * [ncdf4][]
    * [raster][]
    * [rasterVis][]

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[netCDF]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[EDGAR 4.2 overview]: http://edgar.jrc.ec.europa.eu/overview.php?v=42
[EDGAR 4.2 categories]: http://edgar.jrc.ec.europa.eu/methodology.php#12sou
[EDGAR categories in this study]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/N2O_emission_inventories_over_AQMEII-NA_2008#!edgar-42-categories
[CMAQ @ CMAS]: http://www.cmaq-model.org/
[AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz]: ../../downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz
[ncdf4]: http://cran.r-project.org/web/packages/ncdf4/
[raster]: http://cran.r-project.org/web/packages/raster/
[rasterVis]: http://cran.r-project.org/web/packages/rasterVis/

# operation

To run this code,

1. `git clone` this repo. (See commandlines on the project homepage, where you probably are now.)
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. Download a copy of [these bash utilities][bash_utilities.sh] to the working directory.
    1. Open the file in in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. You may also want to open the [driver (bash) script][EDGAR_driver.sh] in an editor and take a look. It should run Out Of The Box, but you might need to tweak something there. In the worst case, you could hardcode your paths and apps in the driver.
    1. Once you've got it working, you may want to fork it. If so, you can automate running your changes with [uber_driver.sh][] (changing that as needed, too).
1. Run the driver:
    `$ ./EDGAR_driver.sh`
1. This will download input, then run
    * an [R script][sum_reunit.r] to sum the raw inputs, convert that intermediate product from mass flux to molar-mass rate, and plot it. The driver should display [a global plot][edgar_non-ag_summed_massed.pdf] if properly configured.
    * an [R script][regrid_global_to_AQMEII.r] to regrid the previous product, and plot the output. The driver should display [an AQMEII plot][edgar_non-ag_summed_massed_regrid.pdf] if properly configured.
    * an [NCL script][retemp.ncl] to [generate hourly output][emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz] suitable for input to [CMAQ][CMAQ @ CMAS].
    * an [NCL script][check_conservation.ncl] to check conservation of mass from input to output. Given that the output domain (AQMEII-NA) is significantly smaller than the input domain (global), it merely reports the fraction of mass (as `kg`) in output vs input, and compares that to an estimate of the land area of the output domain relative to the input domain (since these emissions seem overwhelmingly land-based). Current output includes

                Compare annual emissions reported by EDGAR to our calculated annual emissions:
                           raw input    sum 
                EDGAR      annual sum   calculated
                sector     (kgN2O)      from output   EDGAR/calc
                --------   ----------   -----------   ----------
                combust     3.24e+08     3.23e+08      1.00e+00 
                nonroad     7.79e+07     7.76e+07      1.00e+00 
                road        2.23e+08     2.23e+08      1.00e+00 
                resid       2.85e+08     2.84e+08      1.00e+00 
                petro       6.52e+06     6.52e+06      1.00e+00 
                ind         1.33e+09     1.33e+09      1.00e+00 
                manure      3.36e+08     3.35e+08      1.00e+00 
                runoff      8.94e+08     8.92e+08      1.00e+00 
                waste       3.75e+08     3.74e+08      1.00e+00 
                fff         7.53e+05     7.53e+05      1.00e+00 
                tropo       7.11e+08     6.99e+08      1.02e+00 

                Is N2O conserved from input to output? units=kg N2O
                (note (US land area)/(earth land area) ~= 6.15e-02)
                    input      input     output     output           
                   global        NAs  AQMEII-NA        NAs     out/in
                 4.55e+09          0   4.38e+08          0   9.64e-02

[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[EDGAR_driver.sh]: ../../src/HEAD/EDGAR_driver.sh?at=master
[sum_reunit.r]: ../../src/HEAD/sum_reunit.r?at=master
[regrid_global_to_AQMEII.r]: ../../src/HEAD/regrid_global_to_AQMEII.r?at=master
[retemp.ncl]: ../../src/HEAD/retemp.ncl?at=master
[check_conservation.ncl]: ../../src/HEAD/check_conservation.ncl?at=master
[EDGAR-4.2-minus-soils-and-biomass input processing @ project wiki]: https://github.com/TomRoche/cornbeltN2O/wiki/Simulation-of-N2O-Production-and-Transport-in-the-US-Cornbelt-Compared-to-Tower-Measurements#wiki-input-processing-EDGAR-4.2-minus-soils-and-biomass
[edgar_non-ag_summed_massed.pdf]: ../../downloads/edgar_non-ag_summed_massed.pdf
[edgar_non-ag_summed_massed_regrid.pdf]: ../../downloads/edgar_non-ag_summed_massed_regrid.pdf

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `get_filepath_from_template.ncl`
    * `netCDF.stats.to.stdout.r`
    * `string.ncl`
    * `summarize.ncl`
    * `time.ncl`
    * `visualization.r`
    * refactor more of the common NCL IOAPI-writing code to local [`IOAPI.ncl`][IOAPI.ncl]?
1. Move all these TODOs to [issue tracker][EDGAR-4.2_minus_soil_and_biomass issues].
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. Create common project for `regrid_resources` à la [regrid_utils][], so I don't hafta hunt down which resource is in which project.
1. All regrids: how to nudge off/onshore as required? e.g., soil or burning emissions should never be offshore, marine emissions should never be onshore.
1. All regrid maps: add Caribbean islands (esp Bahamas! for offshore burning), Canadian provinces, Mexican states.
1. Complain to ncl-talk about NCL "unsupported extensions," e.g., `.ncf` and `<null/>` (e.g., MCIP output).
1. Determine why `<-` assignment is occasionally required in calls to `visualize.*(...)`.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[EDGAR-4.2_minus_soil_and_biomass issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[IOAPI.ncl]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/IOAPI.ncl?at=master
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
