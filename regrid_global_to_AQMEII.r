### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to 2D-regrid EDGAR global/unprojected netCDF data to AQMEII-NA, an LCC-projected subdomain

# If running manually in R console, remember to run setup actions: `source ./sum_regrid_retemp_reunit.sh`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

## data

# kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('EDGAR_NONAG_REGRID_HELPER_FN')
this_fn <- my_this_fn

work_dir <- Sys.getenv('WORK_DIR')
out_pdf_fp <- Sys.getenv('EDGAR_NONAG_REGRID_PDF_FP')
pdf_er <- Sys.getenv('PDF_VIEWER')
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS'))

in_fp <- Sys.getenv('EDGAR_NONAG_REUNIT_FP')
in_datavar_name <- Sys.getenv('EDGAR_NONAG_REUNIT_DATAVAR_NAME')
in_datavar_long_name <- Sys.getenv('EDGAR_NONAG_REUNIT_DATAVAR_LONG_NAME')
in_datavar_unit <- Sys.getenv('EDGAR_NONAG_REUNIT_DATAVAR_UNIT')
# in_datavar_na <- as.numeric(Sys.getenv('EDGAR_NONAG_REUNIT_DATAVAR_NA'))
# in_time_dim_name <- Sys.getenv('EDGAR_NONAG_REUNIT_TIME_VAR_NAME')

template_var_name <- Sys.getenv('TEMPLATE_VAR_NAME')
template_input_fp <- Sys.getenv('TEMPLATE_INPUT_FP')

out_fp <- Sys.getenv('EDGAR_NONAG_REGRID_FP')
out_datavar_name <- in_datavar_name
out_datavar_long_name <- in_datavar_long_name
out_datavar_unit <- in_datavar_unit
# out_datavar_na <- raw_datavar_na
out_time_dim_name <- Sys.getenv('EDGAR_NONAG_REGRID_TIME_VAR_NAME')
out_x_var_name <- Sys.getenv('EDGAR_NONAG_REGRID_X_VAR_NAME')
out_y_var_name <- Sys.getenv('EDGAR_NONAG_REGRID_Y_VAR_NAME')
# out_z_var_name <- Sys.getenv('EDGAR_NONAG_REGRID_TIME_VAR_NAME')
# out_z_var_unit <- Sys.getenv('EDGAR_NONAG_REGRID_TIME_VAR_UNIT')

stat_funcs_fp <- Sys.getenv('STATS_FUNCS_FP')
viz_funcs_fp <- Sys.getenv('VIS_FUNCS_FP')

## projection
global_proj4 <- Sys.getenv('GLOBAL_PROJ4')

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

source(stat_funcs_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(viz_funcs_fp)

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson
# http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

## # get a global map
## library(maps)
## map.world.unproj <- maps::map('world', plot=FALSE)

## for output viz

# output coordinate reference system:
# use package=M3 to get CRS from template file
library(M3)
out.proj4 <- M3::get.proj.info.M3(template_input_fp)
# cat(sprintf('out.proj4=%s\n', out.proj4)) # debugging
# out.proj4=+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
out.crs <- sp::CRS(out.proj4)

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# (minimally) process input
# ----------------------------------------------------------------------

library(raster)
in.raster <- raster::raster(in_fp, varname=in_datavar_name)

# # start debugging
# in.raster
# # class       : RasterLayer 
# # dimensions  : 1800, 3600, 6480000  (nrow, ncol, ncell)
# # resolution  : 0.1, 0.1  (x, y)
# # extent      : -180, 180, -90, 90  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0 
# # data source : /home/rtd/code/regridding/EDGAR-4.2_minus_soil_and_biomass/edgar_non-ag_summed_massed.nc 
# # names       : Emissions.of.N2O.. 
# # zvar        : emi_n2o 
# 
# summary(in.raster) # compare to netCDF.stats following
# #         Emissions.of.N2O..
# # Min.          0.000000e+00
# # 1st Qu.       2.737726e-06
# # Median        8.607805e-06
# # 3rd Qu.       3.835558e-05
# # Max.          1.591811e+01
# # NA's          0.000000e+00
# # Warning message:
# # In .local(object, ...) :
# #   summary is an estimate based on a sample of 1e+05 cells (1.54% of all cells)
# 
# netCDF.stats.to.stdout(netcdf.fp=in_fp, data.var.name=in_datavar_name)
# # For /home/rtd/code/regridding/EDGAR-4.2_minus_soil_and_biomass/edgar_non-ag_summed_massed.nc var=emi_n2o
# #       cells=6480000
# #       obs=6480000
# #       min=0
# #       q1=2.72e-06
# #       med=8.59e-06
# #       mean=0.000506
# #       q3=3.79e-05
# #       max=17.2
# #       sum=3.28e+03
# 
# #   end debugging

# ----------------------------------------------------------------------
# regrid
# ----------------------------------------------------------------------

# use package=M3 to get extents from template file (thanks CGN!)
extents.info <- M3::get.grid.info.M3(template_input_fp)
extents.xmin <- extents.info$x.orig
extents.xmax <- max(
  M3::get.coord.for.dimension(
    file=template_input_fp, dimension="col", position="upper", units="m")$coords)
extents.ymin <- extents.info$y.orig
extents.ymax <- max(
  M3::get.coord.for.dimension(
    file=template_input_fp, dimension="row", position="upper", units="m")$coords)
grid.res <- c(extents.info$x.cell.width, extents.info$y.cell.width) # units=m

template.extents <-
  raster::extent(extents.xmin, extents.xmax, extents.ymin, extents.ymax)
# template.extents # debugging
# class       : Extent 
# xmin        : -2556000 
# xmax        : 2952000 
# ymin        : -1728000 
# ymax        : 1860000 

template.in.raster <-
  raster::raster(template_input_fp, varname=template_var_name)
template.raster <- raster::projectExtent(template.in.raster, crs=out.crs)
#> Warning message:
#> In projectExtent(template.in.raster, out.proj4) :
#>   158 projected point(s) not finite
# is that "projected point(s) not finite" warning important? Probably not, per Hijmans

# without this, extents aren't correct!
template.raster@extent <- template.extents
# should resemble the domain specification @
# https://github.com/TomRoche/cornbeltN2O/wiki/AQMEII-North-American-domain#wiki-EPA

# template.raster # debugging
# class       : RasterLayer 
# dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# resolution  : 12000, 12000  (x, y)
# extent      : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
# coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 

# at last: do the regridding
cat(sprintf(  # debugging
  '%s: regridding (may take awhile)\n', this_fn))
out.raster <-
  raster::projectRaster(
    # give a template with extents--fast, but gotta calculate extents
    from=in.raster, to=template.raster, crs=out.crs,
    # give a resolution instead of a template? no, that hangs
#    from=in.raster, res=grid.res, crs=out.proj4,
    method='bilinear', overwrite=TRUE, format='CDF',
    # args from writeRaster
#    NAflag=regrid.datavar.na,
    varname=out_datavar_name, 
    varunit=out_datavar_unit,
    longname=out_datavar_long_name,
    xname=out_x_var_name,
    yname=out_y_var_name,
#    zname=out_z_var_name,
#    zunit=out_z_var_unit,
    filename=out_fp)

# # start debugging
# cat(sprintf(  # debugging
#   '\n\n%s: regridding -> out.raster==\n', this_fn))
# out.raster
# # class       : RasterLayer 
# # dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# # resolution  : 12000, 12000  (x, y)
# # extent      : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 
# # data source : /home/rtd/code/regridding/EDGAR-4.2_minus_soil_and_biomass/edgar_non-ag_summed_massed_regrid.nc 
# # names       : Emissions.of.N2O.. 
# # zvar        : emi_n2o 
# 
# system(sprintf('ls -alht %s | head', work_dir))
# 
# system(sprintf('ncdump -h %s', out_fp))
# # netcdf edgar_non-ag_summed_massed_regrid {
# # dimensions:
# #       COL = 459 ;
# #       ROW = 299 ;
# # variables:
# #       double COL(COL) ;
# #               COL:units = "meter" ;
# #               COL:long_name = "COL" ;
# #       double ROW(ROW) ;
# #               ROW:units = "meter" ;
# #               ROW:long_name = "ROW" ;
# #       float emi_n2o(ROW, COL) ;
# #               emi_n2o:_FillValue = -3.4e+38 ;
# #               emi_n2o:missing_value = -3.4e+38 ;
# #               emi_n2o:long_name = "Emissions of N2O - " ;
# #               emi_n2o:projection = "+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000" ;
# #               emi_n2o:projection_format = "PROJ.4" ;
# #               emi_n2o:min = 7.70719478841151e-06 ;
# #               emi_n2o:max = 1.76586152693827 ;
# #
# # // global attributes:
# #               :Conventions = "CF-1.4" ;
# #               :created_by = "R, packages ncdf and raster (version 2.0-41)" ;
# #               :date = "2013-03-14 16:22:29" ;
# # }
# 
# netCDF.stats.to.stdout(netcdf.fp=out_fp, data.var.name=out_datavar_name)
# # For /home/rtd/code/regridding/EDGAR-4.2_minus_soil_and_biomass/edgar_non-ag_summed_massed_regrid.nc var=emi_n2o
# #       cells=137241
# #       obs=137241
# #       min=7.71e-06
# #       q1=7.06e-05
# #       med=0.000385
# #       mean=0.00229
# #       q3=0.00152
# #       max=1.77
# #       sum=315
# 
# #   end debugging

# ----------------------------------------------------------------------
# visualize output
# ----------------------------------------------------------------------

## get projected North American map
NorAm.shp <- project.NorAm.boundaries.for.CMAQ(
  units='m',
  extents.fp=template_input_fp,
  extents=template.extents,
  LCC.parallels=c(33,45),
  CRS=out.crs)
# # start debugging
# class(NorAm.shp)
# bbox(NorAm.shp)
# # compare bbox to (above)
# # > template.extents
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 
# #   end debugging

# Why does '=' fail and '<-' succeed in ONE PLACE in the arg list?
visualize.layer(
  nc.fp=out_fp,
  layer=out.raster,
  datavar.name=out_datavar_name,
  sigdigs=sigdigs,
#  map.list=list(map.noram.shp.proj, state.shp),
#  map.list <- list(map.noram.shp.proj, state.shp),
  map.list <- list(NorAm.shp),
  pdf.fp=out_pdf_fp,
  pdf.height=10,
  pdf.width=10
)
