;!/usr/bin/env ncl ; requires version >= 5.1.1
;;; ----------------------------------------------------------------------
;;; Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

;;; This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

;;; * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;;; * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

;;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
;;; ----------------------------------------------------------------------

; Convert regridded EDGAR data to the units and temporality required for CMAQ.

;----------------------------------------------------------------------
; libraries
;----------------------------------------------------------------------

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"    ; all built-ins?
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" ; for int2flt,  *_VarAtts

; can't call `getenv` before `begin`?
load "$IOAPI_FUNCS_FP"
load "$SUMMARIZE_FUNCS_FP"

;----------------------------------------------------------------------
; functions
;----------------------------------------------------------------------

;----------------------------------------------------------------------
; code
;----------------------------------------------------------------------

begin ; skip if copy/paste-ing to console

;----------------------------------------------------------------------
; constants
;----------------------------------------------------------------------

  this_fn = getenv("EDGAR_NONAG_RETEMP_HELPER_FN") ; for debugging

  ;; generic model constants
  model_year = stringtoint(getenv("MODEL_YEAR"))

  ;; regridded output from R script is our input
  regrid_dv_name = getenv("EDGAR_NONAG_REGRID_DATAVAR_NAME")
  regrid_fp = getenv("EDGAR_NONAG_REGRID_FP")                 ; path

  ;; create data container files from extents/template file
  template_fp = getenv("TEMPLATE_INPUT_FP")
  template_dv_name = getenv("TEMPLATE_INPUT_DATAVAR_NAME")
  template_TFLAG_name = getenv("TEMPLATE_INPUT_TFLAG_NAME")

  ; driver must
  ; * handle fact that NCL {does not support, won't create} *.ncf :-(
  ; * ensure output_fp_template* do not exist!
;   output_fp_template_CMAQ = getenv("CMAQ_TARGET_FP_TEMPLATE_CMAQ")
;   output_fp_template_NCL = getenv("CMAQ_TARGET_FP_TEMPLATE_NCL")
  output_fp_NCL = getenv("CMAQ_TARGET_FP_NCL")
  output_fp_CMAQ = getenv("CMAQ_TARGET_FP_CMAQ")

;   ; replace output_fp_template_str with <year/><month/> to create "real" filepaths
;   output_fp_template_str = getenv("CMAQ_TARGET_FN_TEMPLATE_STR")
  output_dv_name = getenv("CMAQ_TARGET_DATAVAR_NAME")
  output_attr_history = getenv("CMAQ_TARGET_FILE_ATTR_HISTORY")
  ; datavar metadata
;   output_datavar_attr_long_name = getenv("CMAQ_TARGET_DATAVAR_ATTR_LONG_NAME")
;   output_datavar_attr_units = getenv("CMAQ_TARGET_DATAVAR_ATTR_UNITS")
;   output_datavar_attr_var_desc = getenv("CMAQ_TARGET_DATAVAR_ATTR_VAR_DESC")
  output_attr_filedesc = getenv("CMAQ_TARGET_FILE_ATTR_FILEDESC")
  output_attr_stime = getenv("IOAPI_START_OF_TIMESTEPS")
  output_attr_tstep = getenv("IOAPI_TIMESTEP_LENGTH")

;----------------------------------------------------------------------
; payload
;----------------------------------------------------------------------

  ;;; retemporalize the previously-regridded output
  ;;; reunit-ing not required (done in massification step)
  regrid_fh = addfile(regrid_fp, "r")       ; file handle
  regrid_var = regrid_fh->$regrid_dv_name$  ; datavar
  regrid_arr = regrid_var(:,:)              ; data

; ; start debug---------------------------------------------------------

;   printVarSummary(regrid_var)
; ; Variable: regrid_var
; ; Type: float
; ; Total Size: 548964 bytes
; ;             137241 values
; ; Number of Dimensions: 2
; ; Dimensions and sizes: [ROW | 299] x [COL | 459]
; ; Coordinates: 
; ;             ROW: [1854000..-1722000]
; ;             COL: [-2550000..2946000]
; ; Number Of Attributes: 7
; ;   _FillValue :        -3.4e+38
; ;   missing_value :     -3.4e+38
; ;   long_name : Emissions of N2O - 
; ;   projection :        +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
; ;   projection_format : PROJ.4
; ;   min :       7.707194788411515e-06
; ;   max :       1.765861526938268

;----------------------------------------------------------------------
; fix output rotation 
;----------------------------------------------------------------------

  ;;; note problem above (made stark by viewing in VERDI): rows are reversed from {normal, rest of our outputs}
  ;;; > ; ;             ROW: [1854000..-1722000]
  ;;; gotta fix that!
  row_reversed_arr = regrid_arr(::-1,:)

; ; start debug---------------------------------------------------------
; printVarSummary(row_reversed_arr)
; ; Variable: row_reversed_arr
; ; Type: float
; ; Total Size: 548964 bytes
; ;             137241 values
; ; Number of Dimensions: 2
; ; Dimensions and sizes:	[ROW | 299] x [COL | 459]
; ; Coordinates: 
; ;             ROW: [-1722000..1854000]
; ;             COL: [-2550000..2946000]
; ; Number Of Attributes: 7
; ;   _FillValue :	-3.4e+38
; ;   missing_value :	-3.4e+38
; ;   long_name :	Emissions of N2O - 
; ;   projection :	+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
; ;   projection_format :	PROJ.4
; ;   min :	7.707194788411515e-06
; ;   max :	1.765861526938268
; ;   end debug---------------------------------------------------------

;----------------------------------------------------------------------
; write output as yearly, IOAPI
;----------------------------------------------------------------------

  ;;; see NCL file-writing procedure outline @ http://www.ncl.ucar.edu/Applications/method_2.shtml

  ;;; 1. Setup file handles, datavars
  template_fh = addfile(template_fp, "r")             ; file handle
  template_dv = template_fh->$template_dv_name$       ; real data
  template_TFLAG = template_fh->$template_TFLAG_name$ ; "fake" IOAPI metadatavar
  output_fh = addfile(output_fp_NCL, "c")             ; NCL won't write .ncf
  ; will write datavar=output_dv below

  ;; 1.1. (optional) declare output file definition mode
  setfileoption(output_fh, "DefineMode", True)

  ;;; 2. Define global/file attributes.

  ;; 2.1. Copy global/file attributes from template: see Example 3 in http://www.ncl.ucar.edu/Document/Functions/Built-in/addfile.shtml
  template_fa_names = getvaratts(template_fh)
  if (.not. all(ismissing(template_fa_names))) then
    do i=0, dimsizes(template_fa_names)-1
      fa_name = template_fa_names(i)
      output_fh@$fa_name$ = template_fh@$fa_name$
    end do
  end if

  ;; 2.2. Overwrite non-template global/file attributes (if any).
  output_fh@creation_date = systemfunc("date")
  output_fh@FILEDESC = output_attr_filedesc
  output_fh@HISTORY = output_attr_history
  output_fh@YEAR = model_year
  output_fh@SDATE = stringtoint(get_yearly_SDATE(model_year))
  output_fh@STIME = stringtoint(output_attr_stime)
  output_fh@TSTEP = stringtoint(output_attr_tstep)

  ;;; 3. Define {dimensions, coordinate variables} and their dimensionality

  ;;; 3.1. Copy IOAPI metadatavar=TFLAG dimensions and attributes from template to output.
  template_TFLAG_type = typeof(template_TFLAG)     ; type as string
  template_TFLAG_mv = getFillValue(template_TFLAG) ; missing value

  ;;; Major aside: IOAPI does not apparently typically set attr={_FillValue, missing_value} on datavar=TFLAG, which makes template_TFLAG_mv==NA
  ;;; Below we then propagate that to output_TFLAG, and NCL is good with that.
  ;;; But when we try to use this with R::ncdf4, it causes problems. So set template_TFLAG_mv, and
  ;;; TODO: raise issue on netcdfgroup@unidata.ucar.edu: NCL OK with putting datavar with missing _FillValue, ncdf4 is not

  if (ismissing(template_TFLAG_mv)) then
    template_TFLAG_mv = default_fillvalue(template_TFLAG_type)
  end if
  template_TFLAG_dims_sizes = dimsizes(template_TFLAG)
  template_TFLAG_dims_n = dimsizes(template_TFLAG_dims_sizes) ; == |dims|
  template_TFLAG_dims_names = getvardims(template_TFLAG) ; TSTEP, VAR, DATE-TIME
  vars_n = template_TFLAG_dims_sizes(1)
  datetimes_n = template_TFLAG_dims_sizes(2)

  ;;; 3.2. Copy "real" datavar dimensions from template to output.

  output_dv_type = typeof(template_dv)     ; type as string
  output_dv_mv = getFillValue(template_dv) ; missing value
  if (ismissing(output_dv_mv)) then
    output_dv_mv = default_fillvalue(output_dv_type)
  end if
  output_dv_dims_names = getvardims(template_dv)
  output_dv_dims_sizes = dimsizes(template_dv)
  tsteps_n = output_dv_dims_sizes(0)
  layers_n = output_dv_dims_sizes(1)
  rows_n = output_dv_dims_sizes(2)
  cols_n = output_dv_dims_sizes(3)

  ;;; 3.3. Copy file dimensions from template to output.
  output_fh_dims_names = getvardims(template_fh)
  output_fh_dims_n = dimsizes(output_fh_dims_names)
  output_fh_dims_sizes = new(output_fh_dims_n, integer, "No_FillValue") ; dimensions must never have NA values

  do i_dim = 0 , output_fh_dims_n-1
    i_dim_name = output_fh_dims_names(i_dim)
    if      (i_dim_name .eq. "COL") then
      output_fh_dims_sizes(i_dim) = cols_n
    else if (i_dim_name .eq. "DATE-TIME") then
      output_fh_dims_sizes(i_dim) = datetimes_n
    else if (i_dim_name .eq. "LAY") then
      output_fh_dims_sizes(i_dim) = layers_n
    else if (i_dim_name .eq. "ROW") then
      output_fh_dims_sizes(i_dim) = rows_n
    else if (i_dim_name .eq. "TSTEP") then
      output_fh_dims_sizes(i_dim) = tsteps_n
    else if (i_dim_name .eq. "VAR") then
      output_fh_dims_sizes(i_dim) = vars_n
    end if ; why so many?
    end if  
    end if  
    end if  
    end if  
    end if ; see http://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/NclStatements.shtml#If
  end do

  ;; Should any dimensions be unlimited? Here, no.
  output_fh_dims_unlim = new(output_fh_dims_n, logical, "No_FillValue")
  do i=0 , output_fh_dims_n-1
    output_fh_dims_unlim(i) = False
  end do

  ; at last! define all our dimensions
  filedimdef(output_fh,         \
    (/ output_fh_dims_names /), \
    (/ output_fh_dims_sizes /), \
    (/ output_fh_dims_unlim /)  \
  )

  ;;; 4. Create output datavars.

  ;;; 4.1 TFLAG dimensions and attributes

  output_TFLAG =  new(template_TFLAG_dims_sizes, template_TFLAG_type, template_TFLAG_mv)
  ;; ... copy its dimensions and attributes from template
  copy_VarMeta(template_TFLAG, output_TFLAG)
  ;; define variable on file
  filevardef(output_fh, template_TFLAG_name, template_TFLAG_type, template_TFLAG_dims_names)
  ;; copy attributes to datavar
  filevarattdef(output_fh, template_TFLAG_name, output_TFLAG)

  ;;; 4.2 "real" datavar dimensions and attributes
  output_dv =  new(output_dv_dims_sizes, output_dv_type, output_dv_mv)
  ;; ... copy its dimensions and attributes from template
  copy_VarMeta(template_dv, output_dv) ; our dimensions are not a "leftmost subset"
  ;; define variable on file
  filevardef(output_fh, output_dv_name, output_dv_type, output_dv_dims_names)
  ;; copy attributes to datavar
  filevarattdef(output_fh, output_dv_name, output_dv)
  
  ;; 4.3. (optional) Exit file definition mode
  setfileoption(output_fh, "DefineMode", False)

  ;;; 5. Write data to output variable(s)
  output_fh->$template_TFLAG_name$ = (/ output_TFLAG /)

  ;; Write datavar data (not metadata!) to annual output file: see
  ;; http://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/NclVariables.shtml#ValueOnl

  ;; ...(TSTEP | hour, LAY | 1, ROW | :, COL | :)
  do i_hour = 0 , 24 ; yes, CMAQ days have 25 hours, last is duplicated
    ; since all our hours are the same, write all 25 hours
;     output_fh->$output_dv_name$(i_hour,0,:,:) = (/ regrid_arr(:,:) /)
    output_fh->$output_dv_name$(i_hour,0,:,:) = (/ row_reversed_arr(:,:) /)
  end do

  ;;; 6. Close output filehandle to flush file: see http://www.ncl.ucar.edu/Support/talk_archives/2012/2196.html
  delete(output_fh)

; start debug---------------------------------------------------------

  output_fh = addfile(output_fp_NCL, "r") ; handle
  ; look at the schema
;   printVarSummary(output_fh)
;   printVarSummary(output_fh->$output_dv_name$)
  cmd = "ncdump -h "+output_fp_NCL
  print(this_fn +": about to do: `" +cmd+"`")
  system(cmd)
  ; look at the data
  print(str_get_nl() + this_fn +": about to do: '" +\
    "summarize_4d(output_fh->" + output_dv_name + ")'")
  summarize_4d(output_fh->$output_dv_name$)
  delete(output_fh)

;   end debug---------------------------------------------------------

; ----------------------------------------------------------------------
; view output in VERDI
; ----------------------------------------------------------------------

  ;;; note:
  ;;; * 'False'==foreground process.
  ;;; * Backgrounding in console causes something to vomit on the ampersand ending the generated VERDI call:
  ;;;   it appends ' | more', which fails (?!?)
  ;;;   If so, just copy everything up to the ampersand, and wrap in `system("")
  show_VERDI_fast_tile_plot(output_fp_NCL, output_dv_name, False)

  ;;; cleanup/teardown

  ;;; let the driver do this
;   ; rename the retemped annual file. TODO: make non-system-dependent.
;   cmd = "cp "+output_fp_NCL+" "+output_fp_CMAQ
; ;  print(this_fn+": about to do: '"+cmd+"'") ; debugging
;   system(cmd)

end ; retemp.ncl
